const { defineConfig } = require('cypress')

module.exports = defineConfig({
  'cypress-cucumber-preprocessor': {

    nonGlobalStepDefinitions: false,
    
    step_definitions: './cypress/e2e/login/',
    
  },
  e2e: {
    // // We've imported your old cypress plugins here.
    // // You may want to clean this up later by importing these.
    // setupNodeEvents(on, config) {
    //   return require('./cypress/plugins/index.js')(on, config)

    // },

    setupNodeEvents(on, config) {

      return require('./cypress/plugins/index.js')(on, config)
      
    },
      
    specPattern: 'cypress/e2e/**/*.feature',
      
    supportFile:false,
    experimentalStudio: true
  },
})
