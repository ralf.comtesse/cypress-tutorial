describe('My First Test', () => {
  it('Visits the kitchen sing', () => {
    cy.visit('https://example.cypress.io')

    cy.contains('type').click()

    cy.url().should('include','/commands/action')

    cy.get('.action-email').type('fake@email.com')

    cy.get('.action-email').should('have.value','fake@email.com')
  })

  /* ==== Test Created with Cypress Studio ==== */
  it('testing get', function() {
    /* ==== Generated with Cypress Studio ==== */
    cy.visit('example.cypress.io');
    cy.get(':nth-child(4) > .row > .col-xs-12 > .home-list > :nth-child(1) > ul > :nth-child(1) > a').click();
    cy.get(':nth-child(1) > .col-xs-7').click();
    cy.get('#get > a').should('have.text', 'cy.get()');
    /* ==== End Cypress Studio ==== */
  });
})